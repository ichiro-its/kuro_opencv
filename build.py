#!/usr/bin/env python3

import bushi
import os
import shutil

try:

  opencv_branch = "4.1.2"

  work_path = os.path.dirname(os.path.realpath(__file__))
  os.chdir(work_path)

  if bushi.io.ask_yes_no("do you want to *update the submodules*?"):
    bushi.io.process("updating the *submodules*...")
    if not bushi.tmux.session_run("git submodule update --init --recursive --progress"):
      bushi.io.terminate("failed to *update the submodules*")
    bushi.io.success("*submodules* updated")

    bushi.io.process("checking *build directory*...")
    if os.path.isdir(os.path.join(work_path, "build")):
      bushi.io.process("removing *build directory*...")
      shutil.rmtree(os.path.join(work_path, "build"))
      bushi.io.success("*build directory* removed")

  bushi.io.process("changing *OpenCV* branch...")
  os.chdir(os.path.join(work_path, "modules/opencv"))
  if not bushi.cmd.run_muted("git checkout " + opencv_branch):
    bushi.io.terminate("failed to change *OpenCV* branch to *" + opencv_branch + "*")
  bushi.io.success("*OpenCV* branch changed to *" + opencv_branch + "*")

  bushi.io.process("checking *build directory*...")
  build_dir_path = os.path.join(work_path, "build")
  if not os.path.isdir(build_dir_path):
    bushi.io.process("creating *build directory*...")
    os.makedirs(build_dir_path)
    if not os.path.isdir(build_dir_path):
      bushi.io.terminate("failed to create *build directory*")
    bushi.io.success("*build directory* created")

  bushi.io.process("checking *CMake rules*...")
  makefile_path = os.path.join(work_path, "build/Makefile")
  if not os.path.isfile(makefile_path):
    bushi.io.process("creating *CMake rules*...")
    os.chdir(os.path.join(work_path, "build"))
    command = "cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=../install "
    command += os.path.join(work_path, "modules/opencv")
    if not bushi.tmux.session_run(command):
      bushi.io.terminate("failed to create *CMake rules*")
    bushi.io.success("*CMake rules* created")

  bushi.io.process("checking *install directory*...")
  install_dir_path = os.path.join(work_path, "install")
  if os.path.isdir(install_dir_path):
    bushi.io.process("removing *install directory*...")
    shutil.rmtree(install_dir_path)
    bushi.io.success("*install directory* removed")

  bushi.io.process("building *OpenCV*...")
  os.chdir(os.path.join(work_path, "build"))
  if not bushi.tmux.session_run("make install -j4"):
    bushi.io.terminate("failed to build *OpenCV*")
  bushi.io.success("*OpenCV* built")

  bushi.io.success("done")
  bushi.io.terminate()

except KeyboardInterrupt:
  bushi.io.terminate("keyboard interrupt")